<?php

require 'vendor/autoload.php';

// use Examples\AddressManager\AddressManager;

// $settings = simplexml_load_file(__DIR__ . '/src/AddressManager/resolve.xml');

// $manager = new AddressManager;

// $manager->outputAddresses((string) $settings->resolvedomains);

// use Examples\Products\ShopProduct;

// $dsn = 'sqlite:' . __DIR__ . '/src/Products/shop.db';

// $pdo = new PDO($dsn);

// $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// $obj = ShopProduct::getInstance(1, $pdo);

// var_dump($obj);

// use Examples\Products\ShopProduct;
// use Examples\Writers\TextProductWriter;

// $product1 = new ShopProduct('My Antonia', 'Willa', 'Cather', 5.99);

// $writer = new TextProductWriter;

// $writer->addProduct($product1);

// $writer->write();

// use Examples\Products\ShopProduct;
// use Examples\Writers\XmlProductWriter;

// $product1 = new ShopProduct('My Antonia', 'Willa', 'Cather', 5.99);

// $writer = new XmlProductWriter;

// $writer->addProduct($product1);

// $writer->write();

// use Examples\Products\ShopProduct;
// use Examples\Services\UtilityService;

// $product1 = new ShopProduct('My Antonia', 'Willa', 'Cather', 5.99);

// var_dump($product1->calculateTax(100));

// $utility1 = new UtilityService;

// var_dump($utility1->calculateTax(100));

// use Examples\Products\ShopProduct;

// $product1 = new ShopProduct('My Antonia', 'Willa', 'Cather', 5.99);

// var_dump($product1->calculateTax(100), $product1->generateId());

// use Examples\Services\UtilityService;

// $utility1 = new UtilityService;

// var_dump($utility1->calculateTax(100));

// use Examples\Services\UtilityService;

// $utility1 = new UtilityService;

// var_dump($utility1->calculateTax(100), $utility1->basicTax(100));

// use Examples\Services\UtilityService;

// var_dump(UtilityService::calculateTax(100));

// use Examples\Services\UtilityService;

// $utility1 = new UtilityService;

// var_dump($utility1->calculateTax(100));

// use Examples\Services\UtilityService;

// $utility1 = new UtilityService(100);

// var_dump($utility1->getFinalPrice());

// use Examples\Documents\Document;

// $document = new Document;

// var_dump($document->create());

// use Examples\Documents\User;
// use Examples\Documents\Spreadsheet;

// var_dump(User::create(), Spreadsheet::create());

// use Examples\Conf\Conf;

// try {
//     $conf = new Conf('/tmp/conf.xml');
    
//     print "user: {$conf->get('user')}\n";
//     print "host: {$conf->get('host')}\n";
    
//     $conf->set('pass', 'newpass');
    
//     $conf->write();
// } catch (Exception $e) {
//     die($e->getMessage());
// }

// use Examples\Conf\Runner;

// Runner::init();

// use Examples\Person\Person;

// $p = new Person;

// print $p->name;

// use Examples\Person\Person;

// $p = new Person;

// if (isset($p->name)) {
//     print $p->name;
// }

// use Examples\Person\Person;

// $p = new Person;

// $p->name = 'Alice';

// var_dump($p);

// use Examples\Person\Person;

// $p = new Person;

// $p->name = null;

// var_dump($p);

// use Examples\Person\Person;
// use Examples\Person\PersonWriter;

// $person = new Person(new PersonWriter);

// $person->writeName();

// use Examples\Person\Address;

// $address = new Address('441b Bakers Street');

// print_r($address);

// use Examples\Person\Character;

// $character = new Character('Bob', 44);

// $character->setId(343);

// unset($person);

// use Examples\Person\Character;

// $character = new Character('Bob', 44);

// $character->setId(343);

// $character2 = clone $character;

// var_dump($character, $character2);

// use Examples\Person\Account;
// use Examples\Person\Character;

// $character = new Character('Bob', 44, new Account(200));

// $character->setId(343);

// $character2 = clone $character;

// $character->account->balance += 10;

// var_dump($character, $character2);

// use Examples\Person\Person;
// use Examples\Person\PersonWriter;

// print new Person(new PersonWriter);

// use Examples\Products\Product;
// use Examples\Products\ProcessSale;

// $logger = fn (Product $product) => print "logging ({$product->name})\n";

// $processor = new ProcessSale;

// $processor->registerCallback($logger);

// $processor->sale(new Product('shoes', 6));

// use Examples\Products\Mailer;
// use Examples\Products\Product;
// use Examples\Products\ProcessSale;

// $processor = new ProcessSale;

// $processor->registerCallback([new Mailer, 'doMail']);

// $processor->sale(new Product('shoes', 6));

// use Examples\Products\Product;
// use Examples\Products\Totalizer;
// use Examples\Products\ProcessSale;

// $processor = new ProcessSale;

// $processor->registerCallback(Totalizer::warnAmount());

// $processor->sale(new Product('shoes', 6));

// use Examples\Products\Product;
// use Examples\Products\Totalizer;
// use Examples\Products\ProcessSale;

// $processor = new ProcessSale;

// $processor->registerCallback(Totalizer::warnAmount(8));

// $processor->sale(new Product('shoes', 6));

// use Examples\Products\Product;
// use Examples\Products\Totalizer;
// use Examples\Products\ProcessSale;

// $markup = 3;

// $counter = fn(Product $product) => print "($product->name) marked up price: " .
//     ($product->price + $markup) . "\n";

// $processor = new ProcessSale;

// $processor->registerCallback($counter);

// $processor->sale(new Product('shoes', 6));

// use Examples\Products\Product;
// use Examples\Products\Totalizer;
// use Examples\Products\ProcessSale;

// $processor = new ProcessSale;
// $totalizer = new Totalizer;

// $processor->registerCallback($totalizer->warnAmount(8));

// $processor->sale(new Product('shoes', 6));

// use Examples\Person\Human;
// use Examples\Contracts\HumanWriter;

// (new Human)->output(
//     new class implements HumanWriter {
//         public function write(Human $human): void
//         {
//             print $human->getName() . ' ' . $human->getAge() . "\n";
//         }
//     }
// );

use Examples\Person\Human;
use Examples\Contracts\HumanWriter;

(new Human)->output(
    new class ("/tmp/persondump") implements HumanWriter {
        public function __construct(private string $path)
        {
        }

        public function write(Human $human): void
        {
            file_put_contents(
                $this->path, $human->getName() . ' ' . $human->getAge() . "\n"
            );
        }
    }
);