<?php

namespace Examples\Traits;

trait IdentityTrait
{
    public function generateId(): string
    {
        return uniqid();
    }
}