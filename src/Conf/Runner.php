<?php

namespace Examples\Conf;

use Exception;
use Examples\Conf\Conf;
use Examples\Exceptions\XmlException;
use Examples\Exceptions\ConfException;
use Examples\Exceptions\FileException;

class Runner
{
    public static function init()
    {
        $fh  =fopen('/tmp/log.txt', 'a');

        try {
            fputs($fh, "start\n");

            $conf = new Conf(dirname(__FILE__) . '/conf.broken.xml');

            print "user: {$conf->get('user')} \n";
            print "host: {$conf->get('host')} \n";

            $conf->set('pass', 'newpass');

            $conf->write();

            fputs($fh, "end\n");

            fclose($fh);
        } catch (FileException $e) {
            fputs($fh, "File exception\n");
        } catch (XmlException $e) {
            fputs($fh, "XML exception\n");
        } catch (ConfException $e) {
            fputs($fh, "Conf exception\n");
        } catch (Exception $e) {
            fputs($fh, "General exception\n");
        } finally {
            fputs($fh, "end\n");

            fclose($fh);
        }
    }
}