<?php

namespace Examples\Writers;

use Examples\Writers\ShopProductWriter;

class TextProductWriter extends ShopProductWriter
{
    public function write(): void
    {
        $str = "PRODUCTS:\n";

        foreach ($this->products as $shopProduct) {
            $str .= $shopProduct->getSummaryLine() . "\n";
        }

        print $str;
    }
}