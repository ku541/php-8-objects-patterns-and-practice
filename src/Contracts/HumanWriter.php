<?php

namespace Examples\Contracts;

use Examples\Person\Human;

interface HumanWriter
{
    public function write(Human $human): void;
}