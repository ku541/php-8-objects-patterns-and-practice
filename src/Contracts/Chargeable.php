<?php

namespace Examples\Contracts;

interface Chargeable
{
    public function getPrice(): float;
}