<?php

namespace Examples\Contracts;

interface IdentityObject
{
    public function generateId(): string;
}