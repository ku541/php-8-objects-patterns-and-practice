<?php

namespace Examples\Documents;

class Document extends DomainObject
{
    public static function getGroup(): string
    {
        return 'document';
    }
}