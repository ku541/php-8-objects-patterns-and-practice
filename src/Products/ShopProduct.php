<?php

namespace Examples\Products;

use Examples\Products\CdProduct;
use Examples\Products\BookProduct;
use Examples\Contracts\Chargeable;
use Examples\Traits\IdentityTrait;
use Examples\Traits\PriceUtilities;
use Examples\Contracts\IdentityObject;

class ShopProduct implements Chargeable, IdentityObject
{
    use PriceUtilities,
        IdentityTrait;

    private int $id = 0;

    private int|float $discount = 0;

    public function __construct(
        private string $title,
        private string $producerMainName,
        private string $producerFirstName,
        protected float $price
    ) {
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public static function getInstance(int $id, \PDO $pdo): ?self
    {
        $stmt = $pdo->prepare('select * from products where id = ?');

        $result = $stmt->execute([$id]);

        $row = $stmt->fetch();

        if (empty($row)) {
            return null;
        }

        if ($row['type'] == 'book') {
            $product = new BookProduct(
                $row['title'],
                $row['firstname'],
                $row['mainname'],
                (float) $row['price'],
                (int) $row['numpages']
            );
        } elseif ($row['type'] == 'cd') {
            $product = new CdProduct(
                $row['title'],
                $row['firstname'],
                $row['mainname'],
                (float) $row['price'],
                (int) $row['playlength']
            );
        } else {
            $firstname = $row['firstname'] ?? '';

            $product = new self(
                $row['title'],
                $firstname,
                $row['mainname'],
                (float) $row['price']
            );
        }

        $product->setId((int) $row['id']);

        $product->setDiscount((int) $row['discount']);

        return $product;
    }

    public function setDiscount(int|float $discount): void
    {
        $this->discount = $discount;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getProducerFirstName(): string
    {
        return $this->producerFirstName;
    }

    public function getProducerMainName(): string
    {
        return $this->producerMainName;
    }

    public function getProducer(): string
    {
        return "$this->producerFirstName $this->producerMainName";
    }

    public function getSummaryLine(): string
    {
        $base = "$this->title ( $this->producerMainName, ";

        $base .= "$this->producerFirstName )";

        return $base;
    }

    public function getPrice(): float
    {
        return $this->price - $this->discount;
    }

    public function getDiscount(): int
    {
        return $this->discount;
    }
}