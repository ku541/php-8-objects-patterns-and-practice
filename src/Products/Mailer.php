<?php

namespace Examples\Products;

use Examples\Products\Product;

class Mailer
{
    public function doMail(Product $product): void
    {
        print "mailing ({$product->name})\n";
    }
}