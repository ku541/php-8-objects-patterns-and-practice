<?php

namespace Examples\Products;

use Examples\Products\ShopProduct;

class CdProduct extends ShopProduct
{
    public function __construct(
        string $title,
        string $producerFirstName,
        string $producerMainName,
        int|float $price,
        private int $playLength
    ) {
        parent::__construct(
            $title,
            $producerFirstName,
            $producerMainName,
            $price
        );
    }

    public function getPlayLength(): int
    {
        return $this->playLength;
    }

    public function getSummaryLine(): string
    {
        $base = parent::getSummaryLine();
        
        $base .= ": playing time - $this->playLength";

        return $base;
    }
}