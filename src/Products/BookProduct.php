<?php

namespace Examples\Products;

use Examples\Products\ShopProduct;

class BookProduct extends ShopProduct
{
    public function __construct(
        string $title,
        string $producerFirstName,
        string $producerMainName,
        int|float $price,
        private int $numPages
    ) {
        parent::__construct(
            $title,
            $producerFirstName,
            $producerMainName,
            $price
        );
    }

    public function getNumberOfPages(): int
    {
        return $this->numPages;
    }

    public function getSummaryLine(): string
    {
        $base = parent::getSummaryLine();

        $base .= ": page count - $this->numPages";

        return $base;
    }
}