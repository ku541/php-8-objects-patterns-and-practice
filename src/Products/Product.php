<?php

namespace Examples\Products;

class Product
{
    public function __construct(public string $name, public float $price)
    {
    }
}