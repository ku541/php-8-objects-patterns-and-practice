<?php

namespace Examples\Products;

use Closure;
use Examples\Products\Product;

class Totalizer
{
    private float $count = 0;
    private float $amt = 0;

    public function warnAmount(int $amt): callable
    {
        $this->amt = $amt;

        return Closure::fromCallable([$this, 'processPrice']);
    }

    private function processPrice(Product $product): void
    {
        $this->count += $product->price;

        print "count: $this->count\n";

        if ($this->count > $this->amt) {
            print "reached high price: {$this->count}\n";
        }
    }
}