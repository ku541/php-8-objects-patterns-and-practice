<?php

namespace Examples\Person;

use Examples\Contracts\HumanWriter;

class Human
{
    public function output(HumanWriter $writer)
    {
        $writer->write($this);
    }

    public function getName(): string
    {
        return "Bob";
    }

    public function getAge(): int
    {
        return 44;
    }
}