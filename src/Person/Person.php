<?php

namespace Examples\Person;

class Person
{
    private ?string $pName;

    private ?string $pAge;

    public function __construct(private PersonWriter $writer)
    {
    }

    public function __call(string $method, array $args): mixed
    {
        if (method_exists($this->writer, $method)) {
            return $this->writer->$method($this);
        }
    }

    public function __get(string $property): mixed
    {
        $method = "get$property";

        if (method_exists($this, $method)) {
            return $this->$method();
        }
    }

    public function __isset(string $property): bool
    {
        $method = "get$property";

        return method_exists($this, $method);
    }

    public function __set(string $property, mixed $value): void
    {
        $method = "set$property";

        if (method_exists($this, $method)) {
            $this->$method($value);
        }
    }

    public function __unset(string $property): void
    {
        $method = "set$property";

        if (method_exists($this, $method)) {
            $this->$method(null);
        }
    }

    public function __toString(): string
    {
        $desc = $this->getName() . " (age ";

        $desc .= $this->getAge() . ")";

        return $desc;
    }

    public function getName(): string
    {
        return 'Bob';
    }

    public function getAge(): int
    {
        return 44;
    }

    public function setName(?string $name): void
    {
        $this->pName = $name;
    }

    public function setAge(?int $age): void
    {
        $this->pAge = $age;
    }
}