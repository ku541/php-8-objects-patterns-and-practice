<?php

namespace Examples\Person;

class Account
{
    public function __construct(public float $balance)
    {
    }
}