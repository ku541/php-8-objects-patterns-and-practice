<?php

namespace Examples\Exceptions;

use Exception;
use LibXMLError;

class XmlException extends Exception
{
    public function construct(private LibXMLError $error)
    {
        $shortfile = basename($error->file);

        $msg = "[$shortfile, line $error->line, col $error->column,] $error->message";

        $this->error = $error;

        parent::__construct($msg, $error->code);
    }

    public function getLibXMLError(): LibXMLError
    {
        return $this->error;
    }
}