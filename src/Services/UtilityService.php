<?php

namespace Examples\Services;

// use Examples\Traits\TaxTools;
use Examples\Traits\PriceUtilities;

class UtilityService extends Service
{
    use PriceUtilities {
        PriceUtilities::calculateTax as private;
    }

    public function __construct(private float $price)
    {
    }

    public function getTaxRate(): float
    {
        return 20;
    }

    public function getFinalPrice(): float
    {
        return $this->price + $this->calculateTax($this->price);
    }
}